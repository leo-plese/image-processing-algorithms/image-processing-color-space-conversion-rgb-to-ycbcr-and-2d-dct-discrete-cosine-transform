#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define IMG_DIM 512
#define BLOCK_DIM 8

const int Y_K1_QUANT_TBL[8][8] = {{16,11,10,16,24,40,51,61},
                             {12,12,14,19,26,58,60,55},
                             {14,13,16,24,40,57,69,56},
                             {14,17,22,29,51,87,80,62},
                             {18,22,37,56,68,109,103,77},
                             {24,35,55,64,81,104,113,92},
                             {49,64,78,87,103,121,120,101},
                             {72,92,95,98,112,100,103,99}};

const int CBCR_K2_QUANT_TBL[8][8] = {{17,18,24,47,99,99,99,99},
                                {18,21,26,66,99,99,99,99},
                                {24,26,56,99,99,99,99,99},
                                {47,66,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99}};

typedef struct my_rgb {
    double r, g, b;
} my_rgb;

void rgb2ycbcr(my_rgb *cur_rgb) {
    double r_comp, g_comp, b_comp;
    r_comp = cur_rgb->r;
    g_comp = cur_rgb->g;
    b_comp = cur_rgb->b;

    cur_rgb->r = 0.299*r_comp + 0.587*g_comp + 0.114*b_comp - 128;
    cur_rgb->g = -0.1687*r_comp - 0.3313*g_comp + 0.5*b_comp;
    cur_rgb->b = 0.5*r_comp - 0.4187*g_comp - 0.0813*b_comp;
}

void calc_dct(my_rgb **block_arr, my_rgb **block_coefs_arr) {
    const double C_UV = pow(0.5, 0.5);
    const double PI_OVER_TWO_BLOCK_DIM = M_PI / (2*BLOCK_DIM);
    const double PREFIX_COEF = 2.0 / BLOCK_DIM;
    for (int u=0; u<BLOCK_DIM; u++) {
        double c_u = (u == 0) ? C_UV : 1;
        for (int v=0; v<BLOCK_DIM; v++) {
            double c_v = (v == 0) ? C_UV : 1;

            my_rgb uv_dct_coef;
            uv_dct_coef.r = 0;
            uv_dct_coef.g = 0;
            uv_dct_coef.b = 0;

            for (int i=0; i<BLOCK_DIM; i++) {
                for (int j=0; j<BLOCK_DIM; j++) {
                    my_rgb cur_pix = block_arr[i][j];
                    uv_dct_coef.r += cur_pix.r * cos((2 * i + 1) * u * PI_OVER_TWO_BLOCK_DIM) * cos((2 * j + 1) * v * PI_OVER_TWO_BLOCK_DIM);
                    uv_dct_coef.g += cur_pix.g * cos((2 * i + 1) * u * PI_OVER_TWO_BLOCK_DIM) * cos((2 * j + 1) * v * PI_OVER_TWO_BLOCK_DIM);
                    uv_dct_coef.b += cur_pix.b * cos((2 * i + 1) * u * PI_OVER_TWO_BLOCK_DIM) * cos((2 * j + 1) * v * PI_OVER_TWO_BLOCK_DIM);
                }
            }
            uv_dct_coef.r = PREFIX_COEF * c_u * c_v * uv_dct_coef.r;
            uv_dct_coef.g = PREFIX_COEF * c_u * c_v * uv_dct_coef.g;
            uv_dct_coef.b = PREFIX_COEF * c_u * c_v * uv_dct_coef.b;
            block_coefs_arr[u][v] = uv_dct_coef;
        }
    }

}

void quantize_dct_coefs(my_rgb **block_coefs_arr){
    for (int u=0; u<BLOCK_DIM; u++) {
        for (int v=0; v<BLOCK_DIM; v++) {
            block_coefs_arr[u][v].r = round(block_coefs_arr[u][v].r / Y_K1_QUANT_TBL[u][v]);
            block_coefs_arr[u][v].g = round(block_coefs_arr[u][v].g / CBCR_K2_QUANT_TBL[u][v]);
            block_coefs_arr[u][v].b = round(block_coefs_arr[u][v].b / CBCR_K2_QUANT_TBL[u][v]);
        }
    }
}

void print_results_to_file(FILE *out_fil, my_rgb **block_coefs_arr) {
    for (int i=0; i<BLOCK_DIM; i++) {
        fprintf(out_fil, "%d", (int)block_coefs_arr[i][0].r);
        for (int j=1; j<BLOCK_DIM-1; j++) {
            fprintf(out_fil, " %d", (int)block_coefs_arr[i][j].r);
        }
        fprintf(out_fil, " %d\n", (int)block_coefs_arr[i][BLOCK_DIM-1].r);
    }
    fprintf(out_fil, "\n");

    for (int i=0; i<BLOCK_DIM; i++) {
        fprintf(out_fil, "%d", (int)block_coefs_arr[i][0].g);
        for (int j=1; j<BLOCK_DIM-1; j++) {
            fprintf(out_fil, " %d", (int)block_coefs_arr[i][j].g);
        }
        fprintf(out_fil, " %d\n", (int)block_coefs_arr[i][BLOCK_DIM-1].g);
    }
    fprintf(out_fil, "\n");

    for (int i=0; i<BLOCK_DIM-1; i++) {
        fprintf(out_fil, "%d", (int)block_coefs_arr[i][0].b);
        for (int j=1; j<BLOCK_DIM-1; j++) {
            fprintf(out_fil, " %d", (int)block_coefs_arr[i][j].b);
        }
        fprintf(out_fil, " %d\n", (int)block_coefs_arr[i][BLOCK_DIM-1].b);
    }

    fprintf(out_fil, "%d", (int)block_coefs_arr[BLOCK_DIM-1][0].b);
    for (int j=1; j<BLOCK_DIM; j++) {
        fprintf(out_fil, " %d", (int)block_coefs_arr[BLOCK_DIM-1][j].b);
    }
}

int main(int argc, char **argv) {

    FILE *fil = fopen(argv[1], "rb");

    if (!fil) {
        perror("could not open file");
        return 1;
    }


    unsigned int num_rgb = 0;
    my_rgb **rgb_arr = (my_rgb**) malloc(IMG_DIM*sizeof(my_rgb*));
    for (int i=0; i<IMG_DIM; i++) {
        rgb_arr[i] = (my_rgb*) malloc(IMG_DIM*sizeof(my_rgb));
    }



    int ch;
    int newline_cnt=0;
    while ((ch=fgetc(fil)) != EOF) {
        if (ch == '\n') {
            newline_cnt++;
            if (newline_cnt == 3) {
                break;
            }
        }
    }

    int three_cnt = 0;
    int rn=0, cn=0;
    my_rgb cur_rgb = {-1,-1,-1};
    int TRIPLE_IMG_DIM = IMG_DIM*3;
    while ((ch=fgetc(fil)) != EOF) {
        switch (three_cnt) {
            case 0:
                cur_rgb.r = ch;
                break;
            case 1:
                cur_rgb.g = ch;
                break;
            case 2:
                cur_rgb.b = ch;
                break;
        }

        num_rgb++;
        three_cnt++;
        if (three_cnt == 3) {
            rgb_arr[rn][cn] = cur_rgb;
            if (num_rgb % TRIPLE_IMG_DIM == 0) {
                rn++;
                cn=0;
            } else {
                cn++;
            }
            cur_rgb.r=-1;
            cur_rgb.g=-1;
            cur_rgb.b=-1;
            three_cnt = 0;
        }
    }

    my_rgb **block_arr = (my_rgb**) malloc(BLOCK_DIM*sizeof(my_rgb*));
    for (int i=0; i<BLOCK_DIM; i++) {
        block_arr[i] = (my_rgb*) malloc(BLOCK_DIM*sizeof(my_rgb));
    }

    char *pt;
    int block_nr = strtol(argv[2], &pt, 10);
    const int NUM_BLOCKS_PER_AXIS = IMG_DIM/BLOCK_DIM;
    int block_2d_row = block_nr/NUM_BLOCKS_PER_AXIS;
    int block_2d_col = block_nr - block_2d_row*NUM_BLOCKS_PER_AXIS;
    int row_from = block_2d_row * BLOCK_DIM;
    int col_from = block_2d_col * BLOCK_DIM;
    int row_to = row_from+BLOCK_DIM;
    int col_to = col_from+BLOCK_DIM;


    int b_i = 0, b_j = 0;
    for (int r=row_from; r<row_to; r++) {
        for (int c=col_from; c<col_to; c++) {
            block_arr[b_i][b_j] = rgb_arr[r][c];
            b_j++;
        }
        b_i++;
        b_j=0;
    }


    // RGB -> YCbCr
    // in my_rgb struct now meaning of fields is:
    // r -> Y, g -> Cb, b -> Cr
    for (int i=0;i<BLOCK_DIM;i++) {
        for (int j=0;j<BLOCK_DIM;j++) {


//            //// r/g/b <0 or >255 should NOT happen!!! - check when calcing!
//            if (block_arr[i][j].r < 0 || block_arr[i][j].r > 255 || block_arr[i][j].g < 0 || block_arr[i][j].g > 255 || block_arr[i][j].b < 0 || block_arr[i][j].b > 255) {
//                printf("!!! %d %d : (%f,%f,%f) !!!\n",i,j,block_arr[i][j].r,block_arr[i][j].g,block_arr[i][j].b);
//            }

            // check that R G B are in range 0-255
            // if not, correct
            if (block_arr[i][j].r < 0) {
                block_arr[i][j].r = 0;
//                printf("R0 new rgb = %f %f %f\n",block_arr[i][j].r,block_arr[i][j].g,block_arr[i][j].b);
            } else if (block_arr[i][j].r > 255) {
                block_arr[i][j].r = 255;
//                printf("R255 new rgb = %f %f %f\n",block_arr[i][j].r,block_arr[i][j].g,block_arr[i][j].b);
            }

            if (block_arr[i][j].g < 0) {
                block_arr[i][j].g = 0;
//                printf("G0 new rgb = %f %f %f\n",block_arr[i][j].r,block_arr[i][j].g,block_arr[i][j].b);
            } else if (block_arr[i][j].g > 255) {
                block_arr[i][j].g = 255;
//                printf("G255 new rgb = %f %f %f\n",block_arr[i][j].r,block_arr[i][j].g,block_arr[i][j].b);
            }

            if (block_arr[i][j].b < 0) {
                block_arr[i][j].b = 0;
//                printf("B0 new rgb = %f %f %f\n",block_arr[i][j].r,block_arr[i][j].g,block_arr[i][j].b);
            } else if (block_arr[i][j].b > 255) {
                block_arr[i][j].b = 255;
//                printf("B255 new rgb = %f %f %f\n",block_arr[i][j].r,block_arr[i][j].g,block_arr[i][j].b);
            }


            // RGB -> YCbCr + translation for -128
            rgb2ycbcr(&block_arr[i][j]);


            //// y/cb/cr <-128 or >127 should NOT happen!!! - check when calcing!
//            if (block_arr[i][j].r < -128 || block_arr[i][j].r > 127 || block_arr[i][j].g < -128 || block_arr[i][j].g > 127 || block_arr[i][j].b < -128 || block_arr[i][j].b > 127) {
//                printf("!!! (%f,%f,%f) !!!\n",cur_rgb.r,cur_rgb.g,cur_rgb.b);
//            }

        }

    }

    my_rgb **block_coefs_arr = (my_rgb**) malloc(BLOCK_DIM*sizeof(my_rgb*));
    for (int i=0; i<BLOCK_DIM; i++) {
        block_coefs_arr[i] = (my_rgb*) malloc(BLOCK_DIM*sizeof(my_rgb));
    }

    calc_dct(block_arr, block_coefs_arr);


    quantize_dct_coefs(block_coefs_arr);


    FILE *out_fil = fopen(argv[3], "w");

    if (!out_fil) {
        perror("could not open file");
        return 1;
    }

    print_results_to_file(out_fil, block_coefs_arr);





    fclose(fil);

    for (int i=0; i<IMG_DIM; i++) {
        free(rgb_arr[i]);
    }
    free(rgb_arr);


    return 0;
}
