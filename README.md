# Image Processing Color Space Conversion RGB to YCbCR and 2D DCT Discrete Cosine Transform

Implemented in C.

My lab assignment in Multimedia Architecture and Systems, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: 2021